# Fonts

The fonts used by this software, when the system font isn't sufficient.
Mostly used to display CJK characters.

Unfortunately, I do not have samples with prompts written in languages
other than French, English and Japanese, so no fonts have been added
to handle characters outside these languages.

The fonts in this directory are :

* Noto Sans CJK  
  Author : Google  
  Downloaded from : https://github.com/googlefonts/noto-cjk

* Spoqa Han Sans Neo Typeface  
  Author : Spoqa  
  Downloaded from : https://fontesk.com/spoqa-han-sans-neo-typeface/

